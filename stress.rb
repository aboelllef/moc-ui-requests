#gem install  httparty  before running this file

require 'httparty'
require "pry"
$baseurl = "http://localhost:3000/api/"
# user data should be updated before running the script
# admin and not admin users
users = [
          {email: "user@domain.com",password: "123123"},
          { email: "user40@domain.com",password: "Test1Test"}
        ]



actions = [
  {list:"all_dashboards",single:"get_dashboard",list_key: "dashboards",
    create: "create_dashboard", update: "update_dashboard",
    attr: {en_name: "object_name"},destroy: "delete_dashboard",key: "dashboard"},
  {list:"get_all_categories",single:"get_category",list_key: "categories",
    create: "create_category", update: "update_category",attr: {en_name: "object_name"},destroy: "delete_category",key: "category"},
  {list:"get_all_folders",single:"get_folder",list_key: "folders",
    create: "create_folder", update: "update_folder",attr: {en_name: "object_name"},destroy: "delete_folder",key:"folder"},
  {list:"all_entity_types",single:"get_entity_type",list_key: "entity_types",
    create: "create_entity_type", update: "update_entity_type",attr: {en_name: "object_name"},destroy: "delete_entity_type",key: "entity_type"},
  {list:"all_approvals",single:"get_approval",list_key: "approvals",prefix: "getApprovals",
    create: "create_approval", update: "update_approval",attr: {owner_id:3 ,parent_owner_id: 4, state: "Pending"},destroy: "delete_approval",key: "approval"},
  {list:"all_reminders",single: nil,list_key: "reminders",prefix: "getTaskReminders",
    create: "create_reminder", update: nil,attr: {owner_id: 2, task_id:5, remind_at: Time.now},destroy: "delete_reminder",key:"reminder"},
  {list:"get_all_privacy_levels",single: "get_privacy_level",list_key: "privacy_levels",
    create: "create_privacy_level", update: "update_privacy_level",attr: {en_name:"object name"},destroy: "delete_privacy_level",key: "privacy_level"},
  {list:"get_all_domain_types",single:"get_domain_type",list_key: "domain_types",
    create: "create_domain_type", update: "update_domain_type",attr: {en_name:"object name"},destroy: "delete_domain_type",key: "domain_type"},
  {list:"get_all_entities",single:"get_entity",list_key: "entities",
    create: "create_entity", update: "update_entity",attr: {entity_type: {id:1} , en_name: 'object_name'},destroy: "delete_entity",key: "entity"},
  {list:"get_all_importance_levels",single:"get_importance_level",list_key: "importance_levels",
    create: "create_importance_level", update: "update_importance_level",attr: {en_name:"object name", priority: Random.new.rand(10000000)},destroy: "delete_importance_level",key: "importance_level"},
  {list:"get_all_delivery_methods",single:"get_delivery_method",list_key: "delivery_methods",
    create: "create_delivery_method", update: "update_delivery_method",attr: {en_name: "object_name"},destroy: "delete_delivery_method",key: "delivery_method"},
  {list:"get_all_reference_types",single:"get_reference_type",list_key: "reference_types",
    create: "create_reference_type", update: "update_reference_type",attr: {en_name:"object name"},destroy: "delete_reference_type",key: "reference_type"},
  {list:"get_all_users",single:"get_user",list_key: "users",
    create: "create_user", update: "update_user",attr: {en_name: 'good user ',email: "user#{Random.new.rand(10000000000)}@domain.com",lang: "en", calendar: "gregorian",password: 'Test1Test'},destroy: "delete_user",key:'user'},
  {list:"get_all_correspondence_types",single:"get_correspondence_type",list_key: "correspondence_types",
    create: "create_correspondence_type", update: "update_correspondence_type",attr: {en_name: "object_name"},destroy: "delete_correspondence_type", key: "correspondence_type"},
  {list:"get_all_notification_types",single: nil,list_key: "notification_types"},
  {list:"get_all_notifications",single: nil,list_key: "notifications"},
  {list:"all_contact_types",single:"get_contact_type",list_key: "contact_types",
    create: "create_contact_type", update: "update_contact_type",attr: {en_name: "object_name"},destroy: "delete_contact_type",key: "contact_type"},
  {list:"get_all_correspondence_codes",single:"get_correspondence_code",list_key: "correspondence_codes",
    create: "create_correspondence_code", update: "update_correspondence_code",attr: {"en_name"=>"aaaaaaaaaaaa", "ar_name"=>"", "code_blocks"=>{"0"=>{"en_name"=>"zzzzzzzzzzzzz","block_type" => "Static","entry_type" => "Number"}}},destroy: "delete_correspondence_code",key: "correspondence_code"},
  {list:"get_all_status_types",single:"get_status_type",list_key: "status_types",
    create: "create_status_type", update: "update_status_type",attr: {en_name: "object_name"},destroy: "delete_status_type",key: "status_type"},
  {list:"get_all_audits",single: nil,list_key: "audits"},
  {list:"all_groups",single:"get_group",list_key: "groups",
    create: "create_group", update: "update_group",attr: {en_name:"object_name"},destroy: "delete_group",key:"group"},
  {list:"get_all_correspondence",single:"get_correspondence",list_key: "correspondences"},
  {list:"all_task_types",single:"get_task_type",list_key: "task_types",
    create: "create_task_type", update: "update_task_type",attr: {en_name:"object name"},destroy: "delete_task_type",key: "task_type"},
  {list:"all_task_statuses",single:"get_task_status",list_key: "task_statuses",
    create: "create_task_status", update: "update_task_status",attr: {en_name:"object name"},destroy: "delete_task_status",key: "task_status"},
  {list:"all_tasks",single:"get_task",list_key: "tasks",
    create: "create_task", update: "update_task",attr: {name: "auto tasks"},destroy: "delete_task",key: "task"},
  {list:"all_approval_requests",single: "get_approval_request",list_key: "approval_requests",
    create: "create_approval_request", update: "update_approval_request",attr: {owner_id:2,correspondence_id: 3, state: "Opening",assignees: {a: {user_id:3,user_name: "7oksha"}}},destroy: "delete_approval_request",key:"approval_request"},
  {list:"all_approval_request_states",single: nil,list_key: "approval_request_states"},
  {list:"all_approval_states",single: nil,list_key: "approval_states"}
]
cud_actions = [
  {create: "create_rule", update: nil,attr: {property_list_name: "CORRESPONDENCE_CREATE",group_id: 2},destroy: "delete_rule",key: "rule"},
  # {create: "create_correspondence", update: "update_correspondence",attr: {code: Random.new.rand(1000000000000),owner: Random.new.rand(10000000)},destroy: "delete_correspondence", key: "correspondence"},
]
def list_action action
  if action[:prefix] then method(action[:prefix]).call(action,$token) and return end
  url = $baseurl + action[:list]
  p url
  res = HTTParty.post("#{url}",{body:{remember_token: $token}})
  data = JSON.parse(res.body)
  if data["isAuthorized"]
    id = data[action[:list_key]][-1]["id"]
  else
    p "not allowed"
    id =nil
  end
  id
end
def find_action action, id
  p "#{$baseurl}#{action[:single]}"
  HTTParty.post("#{$baseurl}#{action[:single]}",{body:{remember_token: $token,id: id}})
end

def getApprovals action,token
  url = "#{$baseurl}#{action[:list]}"
  p url
  res = HTTParty.post("#{$baseurl}all_approval_requests",{body:{remember_token: token}})
  id = JSON.parse(res.body)["approval_requests"][-1]["id"]
  res = HTTParty.post(url,{body:{remember_token: token,approval_request_id: id}})
end

def getTaskReminders action,token
  url = "#{$baseurl}#{action[:list]}"
  p url
  res = HTTParty.post("#{$baseurl}all_tasks",{body:{remember_token: token}})
  id = JSON.parse(res.body)["tasks"][-1]["id"]
  res = HTTParty.post(url,{body:{remember_token: token,task_id: id}})
end

def create_action obj
  p "#{$baseurl}#{obj[:create]}"
  return unless obj[:create]
  attributes = {remember_token: $token,obj[:key].to_sym => obj[:attr]}
  # attributes.merge!({"correspondence_code_id"=> 5}) if obj[:key] == "correspondence"
  res = HTTParty.post("#{$baseurl}#{obj[:create]}",{body: attributes})
  # p res.body
end

def update_action obj, id
  return "" unless obj[:update] || obj[:key] == "correspondence"
  p "#{$baseurl}#{obj[:update]}"
  attributes = {remember_token: $token,obj[:key].to_sym => obj[:attr].merge!(id: id)}
  # p attributes
  res = HTTParty.post("#{$baseurl}#{obj[:update]}",{body: attributes})
  # p res.body
end
def delete_action obj, id
  p "#{$baseurl}#{obj[:destroy]}"
  HTTParty.post("#{$baseurl}#{obj[:destroy]}",{body:{remember_token: $token,"#{obj[:key]}_id" => id}})
  # p res.body
end

def run(actions,cud_actions,users)
  users.each do |user|
    p user
    res = HTTParty.post("#{$baseurl}login",{body: user})
    $token = JSON.parse(res.body)["remember_token"]

    # cud_actions.each do |obj|
    #   id =  (obj)
    #   # p " the create object id is #{id}"
    # end

    actions.each do |action|
      create_action(action)
      id = list_action(action)
      if id
        find_action(action, id)
        update_action(action, id)
        delete_action(action, id)
      end
    end
  end


end

1.times do |index|
  run(actions,cud_actions,users)
end



# match '/generate_new_code' => 'correspondence_code#generate_new_code', via: :post
# match '/generate_correspondence_pdf' => 'correspondence#generate_correspondence_pdf', via: :post
# match '/dismiss_notifications' => 'notification#dismiss_notifications', via: :post
# match '/unbookmark' => 'bookmark#destroy', via: :post
# match '/bookmark' => 'bookmark#create', via: :post
# match '/correspondence_search' => 'correspondence#correspondence_search', via: :post
# match '/get_attachment' => 'attachment#attachment', via: :post
# match '/check_code_validation' => 'correspondence#check_code_validation', via: :post
# match '/reset_auto_code_block' => 'correspondence_code#reset_auto_block', via: :post
# match '/get_correspondence_audits' => 'audit#correspondence_audits', via: :post
# match '/delete_shelf_from_correspondence' => 'attachment_shelf#delete_shelf_from_correspondence', via: :post
# match '/create_shelf_to_correspondence' => 'attachment_shelf#create_shelf_to_correspondence', via: :post
# match '/delete_attachment' => 'attachment#delete_attachment', via: :post
# match '/delete_attachment_from_correspondence' => 'attachment#delete_attachment_from_correspondence', via: :post
# match '/group_rules' => 'rule#group_rules', via: :post
# match '/update_my_account' => 'user#update_my_account', via: :post
# match '/get_general_setting' => 'general_setting#general_setting', via: :post
# match '/create_general_setting' => 'general_setting#create_general_setting', via: [:options, :post]
# match '/update_general_setting' => 'general_setting#update_general_setting', via: [:options, :post]
# match '/company_logo' => 'general_setting#company_logo', via: :post
# match '/get_smtp' => 'smtp#smtp', via: :post
# match '/create_smtp' => 'smtp#create_smtp', via: :post
# match '/update_smtp' => 'smtp#update_smtp', via: :post
# match '/duplicate_correspondence' => 'correspondence#duplicate_correspondence', via: :post
# match '/test_smtp' => 'smtp#test_smtp', via: :post
###########################################
# match '/subject_suggestions' => 'correspondence#subject_suggestions', via: :post
# match '/suggest_users' => 'correspondence#suggest_users', via: :post
# match '/get_my_managers' => 'user#my_managers', via: :post
# match '/get_user_widgets' => 'user#get_user_widgets', via: :post
# match '/chartable_hash' => 'dashboard#chartable_hash', via: :post
# match '/correspondences_total_pages' => 'correspondence#correspondences_total_pages', via: :post
# match '/approval_requests_total_pages' => 'approval_request#approval_requests_total_pages', via: :post
# match '/tasks_total_pages' => 'task#tasks_total_pages', via: :post
# match '/attachment_upload' => 'attachment#attachment_upload', via: [:options, :post]
# match '/attachment_upload_to_correspondence' => 'attachment#attachment_upload_to_correspondence', via: [:options, :post]